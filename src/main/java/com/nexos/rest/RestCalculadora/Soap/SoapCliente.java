package com.nexos.rest.RestCalculadora.Soap;

import com.calculadora.ws.Calculadora;
import com.calculadora.ws.CalculadoraImplService;
import com.nexos.rest.RestCalculadora.modelo.TipoOperacion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

import java.util.ArrayList;

@Service
public class SoapCliente extends WebServiceGatewaySupport {

    Logger logger = LoggerFactory.getLogger(SoapCliente.class);

    /***
     * Realizamos instancias a la libreria del WebServices
     */
    CalculadoraImplService request = new CalculadoraImplService();
    Calculadora consumer = request.getCalculadoraImplPort();

    TipoOperacion resultado = new TipoOperacion();

    public TipoOperacion calcularPendiente(double primerValor, double segundoValor, double tercerValor, double cuartoValor) {
        logger.info("Ingreso al método calcularPendiente");
        double resultadoFinal = 0.0d;
        double resultadoSuperior = 0.0d;
        double resultadoInferior = 0.0d;
        //Se consume el cliente del WS - Resta
        resultadoSuperior = consumer.operacion(2, primerValor, segundoValor);
        resultadoInferior = consumer.operacion(2, tercerValor, cuartoValor);
        if (resultadoSuperior != 0.0 && resultadoInferior != 0.0) {
            //Se consume el cliente del WS - Division
            resultadoFinal = (consumer.operacion(4,resultadoSuperior,resultadoInferior));
            resultado.setResultadoRest(resultadoFinal);
        }
        logger.info("Finalizo método calcularPendiente");
        return resultado;
    }

    public TipoOperacion calcularPromedio(double primerValor, double segundoValor, double tercerValor, double cuartoValor) {
        logger.info("Ingreso al método calcularPromedio");

        ArrayList<Double> notas = new ArrayList();
        double resultadoFinal = 0.0d;
        double resultadoPar = 0.0d;
        double resultadoImpar = 0.0d;
        if(primerValor != 0.0){
            notas.add(primerValor);
        }
        if(segundoValor != 0.0){
            notas.add(segundoValor);
        }
        if(tercerValor != 0.0){
            notas.add(tercerValor);
        }
        if(cuartoValor != 0.0){
            notas.add(cuartoValor);
        }
        if(notas.size() == 1){
            resultadoPar =   consumer.operacion(1, notas.get(0),  0.0);
            resultadoImpar =   consumer.operacion(1, 0.0,  0.0);
            resultadoFinal = (consumer.operacion(1, resultadoPar, resultadoImpar));
            resultado.setResultadoRest(consumer.operacion(4, resultadoFinal,  1));
        }
        if(notas.size() == 2){
            resultadoPar =   consumer.operacion(1, notas.get(0),  notas.get(1));
            resultado.setResultadoRest(consumer.operacion(4, resultadoPar,  2));
        }
        if(notas.size() == 3){
            resultadoPar =   consumer.operacion(1, notas.get(0),  notas.get(1));
            resultadoImpar =   consumer.operacion(1, notas.get(2),  0.0);
            resultadoFinal = (consumer.operacion(1, resultadoPar, resultadoImpar));
            resultado.setResultadoRest(consumer.operacion(4, resultadoPar,  3));
        }
        if(notas.size() == 4){
            resultadoPar =   consumer.operacion(1, notas.get(0),  notas.get(1));
            resultadoImpar =   consumer.operacion(1, notas.get(2), notas.get(3));
            resultadoFinal = (consumer.operacion(1, resultadoPar, resultadoImpar));
            resultado.setResultadoRest(consumer.operacion(4, resultadoFinal,  4));
        }

        logger.info("Finalizo al método calcularPromedio");

        return resultado;
    }

    public TipoOperacion calcularAreaTriangulo(double base, double altura) {
        logger.info("Ingreso al método calcularAreaTriangulo");
        double resultadoWS = 0.0d;
        //Se consume el cliente del WS - Multiplicación
        resultadoWS = consumer.operacion(3, base, altura);
        if (resultadoWS != 0.0d) {
            //Se consume el cliente del WS - Division
            resultado.setResultadoRest(consumer.operacion(4,resultadoWS,2));
        }
        logger.info("Ingreso al método calcularAreaTriangulo");
        return resultado;
    }

    public TipoOperacion calcularAreaCirculo(double base, double exponente) {
        logger.info("Ingreso al método calcularAreaCirculo");
        double valorPI = Math.PI;
        double valorPotencia = Math.pow(base, exponente);
        double resultadoWS = 0.0d;
        if (valorPotencia > 0) {
            //Se consume el cliente del WS - Multiplicacion
            resultadoWS = consumer.operacion(3, valorPI, valorPotencia);
        }

        if (resultadoWS != 0.0d) {
            resultado.setResultadoRest(resultadoWS);
        }
        logger.info("Ingreso al método calcularAreaCirculo");
        return resultado;
    }


}
