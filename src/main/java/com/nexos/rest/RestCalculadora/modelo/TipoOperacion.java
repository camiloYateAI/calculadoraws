package com.nexos.rest.RestCalculadora.modelo;

public class TipoOperacion {


    private int operacion;

    private double primerValor;


    private double segundoValor;

    private double tercerValor;
    private double cuartoValor;

    private double resultadoWS;

    private double resultadoRest;


    public TipoOperacion() {
        super();
    }

    public int getOperacion() {
        return operacion;
    }

    public void setOperacion(int operacion) {
        this.operacion = operacion;
    }

    public double getPrimerValor() {
        return primerValor;
    }

    public void setPrimerValor(double primerValor) {
        this.primerValor = primerValor;
    }

    public double getSegundoValor() {
        return segundoValor;
    }

    public void setSegundoValor(double segundoValor) {
        this.segundoValor = segundoValor;
    }

    public double getResultadoWS() {
        return resultadoWS;
    }

    public void setResultadoWS(double resultadoWS) {
        this.resultadoWS = resultadoWS;
    }

    public double getResultadoRest() {
        return resultadoRest;
    }

    public void setResultadoRest(double resultadoRest) {
        this.resultadoRest = resultadoRest;
    }

    public double getTercerValor() {
        return tercerValor;
    }

    public void setTercerValor(double tercerValor) {
        this.tercerValor = tercerValor;
    }

    public double getCuartoValor() {
        return cuartoValor;
    }

    public void setCuartoValor(double cuartoValor) {
        this.cuartoValor = cuartoValor;
    }
}
